<?php

    require_once("mobil.php");
    require_once("bus.php");
    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");


    $mobil = new Mobil("Avanza");
    echo "Nama Mobil = " . $mobil->name . "<br>";
    echo "Jumlah Roda = " . $mobil->roda . "<br>";
    echo "Bahan Bakar = " . $mobil->bahanbakar . "<br>";
    echo "Jumlah Spion = " . $mobil->spion . "<br><br>";

    $bus = new Bus ("Mini Bus");
    echo "Nama Mobil = " . $bus->name . "<br>";
    echo "Jumlah Roda = " . $bus->roda . "<br>";
    echo "Bahan Bakar = " . $bus->bahanbakar . "<br>";
    echo "Jumlah Spion = " . $bus->spion . "<br>";
    echo $bus->Jalan() . "<br><br>"; 

    $animal = new Animal ("Shaun");
    echo "Name = " . $animal->name . "<br>";
    echo "Legs = " . $animal->legs . "<br>";
    echo "Cool Blooded = " . $animal->coldblooded . "<br> <br>";

    $frog = new Buduk ("Buduk");
    echo "Name = " . $frog->name . "<br>";
    echo "Legs = " . $frog->legs . "<br>";
    echo "Cool Blooded = " . $frog->coldblooded . "<br>";
    echo "Jump = " . $frog->jump . "<br><br>";

    $ape = new Sungokong ("Kera Sakti");
    echo "Name = " . $ape->name . "<br>";
    echo "Legs = " . $ape->legs . "<br>";
    echo "Cool Blooded = " . $ape->coldblooded . "<br>";
    echo "Yell = " . $ape->yell . "<br><br>";

?>